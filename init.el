;; -*-Lisp-*-
;; Heikki Lehvaslaiho 
;; Time-stamp: <2013-10-22 09:56:04 heikki> 
;;


;; default font
(set-face-attribute 'default nil :family "Monaco")
(set-face-attribute 'default nil :height 135)

;; Mac OS path for LaTeX an other binaries
(when (eq system-type 'darwin) 
  (setenv "PATH"
	  (concat (getenv "PATH")
		  ":/usr/local/bin:/usr/texbin"))
  (setq exec-path 
        (append exec-path
                '("/usr/local/bin" 
                  "/usr/local/texbin")))

  ;; the PATH variable in Macs to read in local shell setup
  ;; (e.g. ~/.bash_alias)
  (defun set-exec-path-from-shell-PATH ()
    (let ((path-from-shell
           (shell-command-to-string "$SHELL -i -c 'echo $PATH'")))
      (setenv "PATH" path-from-shell)
      (setq exec-path (split-string path-from-shell path-separator))))

  (when window-system (set-exec-path-from-shell-PATH))

  ;;
  ;; locate not in OSX, use mdfind
  ;;
  (setq locate-command "mdfind"))



;; where to manually put new emacs modules
(add-to-list 'load-path "~/.emacs.d/elisp")

;; where to look for emacs configuration files
;; from: http://porkmail.org/elisp/my-site-start/my-site-start.el
(autoload 'my-site-start "~/.emacs.d/elisp/my-site-start" nil t)
(my-site-start "~/.emacs.d/site-start.d/")

(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(add-log-full-name "Heikki Lehvaslaiho")
 '(add-log-mailing-address "heikki.lehvaslaiho@gmail.com")
 '(before-save-hook (quote (time-stamp)))
 '(column-number-mode t)
 '(cua-mode t nil (cua-base))
 '(display-time-24hr-format t)
 '(display-time-mode nil)
 '(require-final-newline t)
 '(save-place t nil (saveplace))
 '(scroll-bar-mode (quote right))
 '(send-mail-function (quote smtpmail-send-it))
 '(smtpmail-smtp-server "smtp.gmail.com")
 '(smtpmail-smtp-service 587)
 '(text-mode-hook (quote (turn-on-auto-fill
                          (lambda nil (flyspell-mode 1)) text-mode-hook-identify)))
 '(uniquify-buffer-name-style (quote forward) nil (uniquify))
 '(user-full-name "Heikki Lehvaslaiho")
 '(user-mail-address "heikki.lehvaslaiho@gmail.com"))


;; safequard against accidental quit
(setq confirm-kill-emacs 'y-or-n-p)

;; emacsify the Mac Command key, not yet
;;(setq mac-command-modifier 'meta)
;;(global-set-key (kbd "M-`") 'other-frame)

;; no more backup files - do I dare to do this?
;;(setq make-backup-files nil)


;;
;; Emacs packages
;;
;; The default gnu repository is http://elpa.gnu.org/packages/
;; we are assuming here (>= emacs-major-version 24)

(require 'package)
;; add MELPA packages to emacs 24: Options/Manage Emacs Packages
;; should replace unreliable marmelade
(add-to-list 'package-archives
             '("melpa" . "http://melpa.milkbox.net/packages/") t)

;; add marmelade packages to emacs 24: Options/Manage Emacs Packages
;;(add-to-list 'package-archives 
;;	     '("marmalade" . "http://marmalade-repo.org/packages/"))
(package-initialize)
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )

;; automate packages to install if moving to a new computer
(defvar my-packages '(auctex
		      auto-complete
		      color-theme
		      color-theme-sanityinc-tomorrow
		      color-theme-solarized
                      clojure-mode
                      clojure-test-mode
                      cperl-mode
                      dash
		      deft
		      dired-details
		      ein
		      git-commit-mode
		      gitconfig-mode
		      gitignore-mode
		      google-contacts
                      ido-vertical-mode
                      iedit
		      magit
		      markdown-mode
                      nrepl
		      oauth2
		      org
		      org-cua-dwim
		      org-magit
                      plenv
		      popup
                      projectile
                      slime
		      starter-kit
                      starter-kit-lisp
                      starter-kit-bindings
                      starter-kit-eshell
                      use-package
                      yaml-mode))

(dolist (p my-packages)
  (when (not (package-installed-p p))
    (package-install p)))
