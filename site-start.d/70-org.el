;;
;; Heikki Lehvaslaiho 
;; Time-stamp: <2013-09-21 18:43:35 heikki> 
;;
;;
;; org
;;

;; hack to get capture working
(add-to-list 'load-path "~/.emacs.d/elpa/org")



;; set the org auto mode
(require 'org)
(add-to-list 'auto-mode-alist '("\\.\\([o][r][g]\\)\\'" . org-mode))

;; org-cua-dwim package installed
;;(require 'org-cua-dwim)
;;(org-cua-dwim-activate)

;; add timestamp to TODO items
(setq org-log-done 'time)

;; locate external jars
(setq org-ditaa-jar-path "~/src/org-mode/contrib/scripts/ditaa.jar")
(setq org-plantuml-jar-path
      (expand-file-name "~/.emacs.d/bin/plantuml.jar"))
;; Use fundamental mode when editing plantuml blocks with C-c '
(add-to-list 'org-src-lang-modes (quote ("plantuml" . fundamental)))

(add-hook 'org-babel-after-execute-hook 'bh/display-inline-images 'append)

(defun bh/display-inline-images ()
  (condition-case nil
      (org-display-inline-images)
    (error nil)))

(org-babel-do-load-languages
 'org-babel-load-languages
  '( (perl . t)
     (clojure . t)
     (R . t)
     (ruby . t)
     (sh . t)
     (python . t)
     (emacs-lisp . t)
     (ditaa . t)
     (plantuml . t)
     (dot . t)
     (lilypond nil)
     (latex . t)
     (sql . t)
     (sqlite . t)
     (org . t)
   ))

;; font colorization according to language
(setq org-src-fontify-natively t)

;; run code without confimation
(setq org-confirm-babel-evaluate nil)



;;
;; latex export from org
;;

(require 'ox-latex) 
(require 'ox-beamer)

;; * My Headline
;; In section [[My Headline]] we discuss ...
(setq org-latex-hyperref-format "\\ref{%s}")
 

;; syntax lighlighting with minted
;; needs pygments installed
;; http://praveen.kumar.in/2012/03/10/org-mode-latex-and-minted-syntax-highlighting/
(setq org-latex-listings 'minted)
(add-to-list 'org-latex-packages-alist '("" "minted"))


(setq texcmd "latexmk -interaction=nonstopmode -interaction=nonstopmode -shell-escape -pdflatex=xelatex -gg -f -pdf %f")


(add-to-list 'org-latex-classes
	     '("fu-org-article"
"\\documentclass[11pt,a4paper]{article}
\\usepackage[T1]{fontenc}
\\usepackage{xunicode}        % for XeTex
\\usepackage{fontspec}        % for XeTex
\\usepackage{xltxtra}         % for XeTex
\\usepackage{url}             % for XeTex to break long URLs at line ending
\\usepackage[english]{babel}  % for XeTex
\\usepackage{libertine} 
\\usepackage{graphicx}
\\usepackage{minted}
\\usepackage{hypernat}
\\usepackage[round]{natbib}

\\usepackage{paralist}
\\let\\itemize\\compactitem
\\let\\description\\compactdesc
\\let\\enumerate\\compactenum  

\\defaultfontfeatures{Mapping=tex-text}
\\setromanfont[Mapping=tex-text, Numbers=OldStyle]{Linux Libertine O} 
 
\\usepackage[a4paper, textheight=10in,
	    marginparsep=7pt, marginparwidth=.6in]{geometry}

\\usepackage[colorlinks=true,urlcolor=black,linkcolor=black,citecolor=blue]{hyperref}

\\let\\nofiles\\relax % Void the \\nofiles command

\\pagestyle{plain}
\\title{}
      [NO-DEFAULT-PACKAGES]
      [NO-PACKAGES]"
	       ("\\section{%s}" . "\\section*{%s}")
	       ("\\subsection{%s}" . "\\subsection*{%s}")
	       ("\\subsubsection{%s}" . "\\subsubsection*{%s}")
	       ("\\paragraph{%s}" . "\\paragraph*{%s}")
	       ("\\subparagraph{%s}" . "\\subparagraph*{%s}")
	       )
	     )

(setq org-latex-pdf-process
  '("/usr/texbin/latexmk -interaction=nonstopmode -shell-escape -pdflatex=/usr/texbin/xelatex -f -pdf %f"))

;;
;; end of latex export

;;
;; Skeletons
;;

;; sblk - Generic block #+BEGIN_SRC FOO .. #+END_SRC
(define-skeleton skel-org-block
  "Insert an org block, querying for type."
  "Type: "
  "#+BEGIN_SRC " str "\n"
  _ -
  "\n#+END_SRC\n")

;; pblk - Perl block #+BEGIN_SRC perl .. #+END_SRC
(define-skeleton skel-org-block-perl
  "Insert an org perl block with results in the output."
  ""
  "#+HEADERS: :results output :exports both :shebang \"#!/usr/bin/env perl\"\n#+BEGIN_SRC perl :tangle yes\n"
  _ - 
  "\n#+END_SRC\n")

(define-abbrev org-mode-abbrev-table "pblk" "" 'skel-org-block-perl)

;; rblk - R block #+BEGIN_SRC R .. #+END_SRC
;;    for optimised for inline graphics 
(define-skeleton skel-org-block-r
  "Insert an org R block with results in the output."
  ""
  "#+HEADER: :session *R* :cache yes :results output graphics :exports both :tangle yes\n#+BEGIN_SRC R  :file a.png  :width 500 :height 500\n"
  _ - 
  "\n#+END_SRC\n")

(define-abbrev org-mode-abbrev-table "rblk" "" 'skel-org-block-r)


;; splantuml - PlantUML Source block
(define-skeleton skel-org-block-plantuml
  "Insert a org plantuml block, querying for filename."
  "File (no extension): "
  "#+BEGIN_SRC plantuml :file " str ".png\n"
  _ -
  "\n#+END_SRC\n")

(define-abbrev org-mode-abbrev-table "splantuml" "" 'skel-org-block-plantuml)

;; sdot - Graphviz DOT block
(define-skeleton skel-org-block-dot
  "Insert a org graphviz dot block, querying for filename."
  "File (no extension): "
  "#+BEGIN_SRC dot :file " str ".png :cmdline -Kdot -Tpng\n"
  "graph G {\n"
  _ - \n
  "}"
  "\n#+END_SRC\n")

(define-abbrev org-mode-abbrev-table "sdot" "" 'skel-org-block-dot)

;; sditaa - Ditaa source block
(define-skeleton skel-org-block-ditaa
  "Insert a org ditaa block, querying for filename."
  "File (no extension): "
  "#+BEGIN_SRC ditaa :file " str ".png  :cmdline -r\n"
  _ -
  "\n#+END_SRC\n")

(define-abbrev org-mode-abbrev-table "sditaa" "" 'skel-org-block-ditaa)

;; lblk - Emacs Lisp source block
(define-skeleton skel-org-block-elisp
  "Insert a org emacs-lisp block"
  ""
  "#+BEGIN_SRC emacs-lisp\n"
  _ -
  "\n#+END_SRC\n")

(define-abbrev org-mode-abbrev-table "lblk" "" 'skel-org-block-elisp)



;
; RefTex mode configuration
;

(setq reftex-default-bibliography
      (quote
       ("/Users/heikki/Documents/latex/all.bib")))

(defun org-mode-reftex-setup ()
  (load-library "reftex")
  (and (buffer-file-name) (file-exists-p (buffer-file-name))
       (progn
	 ;enable auto-revert-mode to update reftex when bibtex file changes
	 (global-auto-revert-mode t)
       (reftex-parse-all))
  (define-key org-mode-map (kbd "C-c )") 'reftex-citation)
  (define-key org-mode-map (kbd "C-c (") 'org-mode-reftex-search))
  )
(add-hook 'org-mode-hook 'org-mode-reftex-setup)


;; Add these two lines to the end of the document. 
;; Change bibfilename to bib file name
;; natbib ref: http://merkel.zoneo.net/Latex/natbib.php
;;.
;;\bibliographystyle{natbib}{}
;;\bibliography{bibfilename}


;; obiblio - bibliography creation block
(define-skeleton skel-org-block-bibl
  "Insert a org/lisp bibliography block"
  ""
  "\n\\bibliographystyle{plainnat}\n\\bibliography{/Users/heikki/Documents/latex/all}\n"
  _ -
  "")

(define-abbrev org-mode-abbrev-table "obiblio" "" 'skel-org-block-bibl)

;;
;; capture
;;
;; see http://doc.norang.ca/org-mode.html#Capture

(setq org-directory "~/Dropbox/org")
(setq org-default-notes-file "~/Dropbox/org/refile.org")
;(setq org-default-notes-file (concat org-directory "~/Dropbox/org/notes.org"))
(define-key global-map "\C-cc" 'org-capture)

;; Capture templates for: TODO tasks, Notes, appointments, phone calls, and org-protocol
(setq org-capture-templates
       (quote (("t" "todo" entry (file "~/Dropbox/org/refile.org")
               "* TODO %?\n%U\n%a\n" :clock-in t :clock-resume t)
              ("r" "respond" entry (file "~/Dropbox/org/refile.org")
               "* NEXT Respond to %:from on %:subject\nSCHEDULED: %t\n%U\n%a\n" :clock-in t :clock-resume t :immediate-finish t)
              ("n" "note" entry (file "~/Dropbox/org/refile.org")
               "* %? :NOTE:\n%U\n%a\n" :clock-in t :clock-resume t)
	      ("j" "journal" entry (file+datetree "~/Dropbox/org/journal.org")
               "* %?\n%U\n" :clock-in t :clock-resume t)
	      ("f" "reFerence" entry (file+datetree "~/Dropbox/org/reference.org")
	       "* %?\nEntered on %U\n  %i")  ; '\n %a' removed as unnecessary
              ("p" "Phone call" entry (file "~/Dropbox/org/refile.org")
               "* PHONE %? :PHONE:\n%U" :clock-in t :clock-resume t)
              ("h" "Habit" entry (file "~/Dropbox/org/refile.org")
               "* NEXT %?\n%U\n%a\nSCHEDULED: %(format-time-string \"<%Y-%m-%d %a .+1d/3d>\")\n:PROPERTIES:\n:STYLE: habit\n:REPEAT_TO_STATE: NEXT\n:END:\n"))))

;; Remove empty LOGBOOK drawers on clock out
(defun bh/remove-empty-drawer-on-clock-out ()
  (interactive)
  (save-excursion
    (beginning-of-line 0)
    (org-remove-empty-drawer-at "LOGBOOK" (point))))

(add-hook 'org-clock-out-hook 'bh/remove-empty-drawer-on-clock-out 'append)


;;
;; refile
;;
;; Targets include this file and any file contributing to the agenda - up to 9 levels deep
(setq org-refile-targets (quote ((nil :maxlevel . 9)
                                 (org-agenda-files :maxlevel . 9))))

;; Use full outline paths for refile targets - we file directly with IDO
(setq org-refile-use-outline-path t)

;; Targets complete directly with IDO
(setq org-outline-path-complete-in-steps nil)

;; Allow refile to create parent tasks with confirmation
(setq org-refile-allow-creating-parent-nodes (quote confirm))

;; Use IDO for both buffer and file completion and ido-everywhere to t
(setq org-completion-use-ido t)
(setq ido-everywhere t)
(setq ido-max-directory-size 100000)
(ido-mode (quote both))

;; Refile settings
;; Exclude DONE state tasks from refile targets
;;(defun bh/verify-refile-target ()
;;  "Exclude todo keywords with a done state from refile targets"
;;  (not (member (nth 2 (org-heading-components)) org-done-keywords)))
;;
;;(setq org-refile-target-verify-function 'bh/verify-refile-target)


;;
;;  agenda
;;
;;
;; Files to search using agenda commands
;;
;;(setq org-agenda-files '("~/Dropbox/org"))  ; all files in the directory
;;'(org-agenda-files (quote ("~/Dropbox/org/actions.org" "~/Dropbox/org/reference.org")))
;;
;; search only reference.org
;; I am not using other files/templates at the moment
'(org-agenda-files (quote ("~/Dropbox/org/reference.org")))


(setq org-agenda-include-diary t)
(setq org-agenda-start-on-weekday 7)
(setq org-tag-alist (quote (("BITCOIN" . ?a)
                            ("BIO"     . ?b)
                            ("COMP"    . ?c)
			    ("EMACS"   . ?e)
			    ("PHOTO"   . ?f)
                            ("GIT"     . ?g)
                            ("HOME"    . ?h)
                            ("LATEX"   . ?l)
			    ("MAP"     . ?m)
                            ("ORG"     . ?o)
                            ("PERL"    . ?p)
			    ("WORK"    . ?w)
                            ("PYTHON"  . ?y)
                            ("ZSH"     . ?z)
			    )))
(global-set-key "\C-cl" 'org-store-link) ; store current position (copy); use C-c C-l 
(global-set-key "\C-ca" 'org-agenda)




;;
;; Access org-mode info with: C-c o
;;
;; Caveat: links to builtin (old) org docs
;;
(defvar fp-org-distribution "~/src/org-mode")
(defun fp-org-info ()
  (interactive)
  (require 'info)
  (let ((Info-directory-list (cons (concat fp-org-distribution "/doc")
                                   Info-directory-list)))
    (org-info)))
(global-set-key "\C-co" 'fp-org-info)



;; reveal export mode
;; https://github.com/yjwen/org-reveal/blob/master/Readme.org
;; cd ~/src/
;; git clone https://github.com/yjwen/org-reveal.git

(require 'ox-reveal)
(setq org-reveal-root "file:///Users/heikki/src/reveal.js")



;; org-mode asks for a master file when opening the first org file in emacs
;; discussion and fix at:
;; http://lists.gnu.org/archive/html/emacs-orgmode/2012-02/msg00759.html

(setq TeX-auto-save t)
(setq TeX-parse-self t)
(setq-default TeX-master nil)
(make-variable-buffer-local 'TeX-master) ;; I think this is need because the variable is not buffer local until Auctex is active

(defun org-mode-reftex-setup ()
  (setq TeX-master t)
  (load-library "reftex")
  (and (buffer-file-name)
       (file-exists-p (buffer-file-name))
       (progn 
     (reftex-parse-all)
     (reftex-set-cite-format "[[cite:%l][%l]]")))
  (define-key org-mode-map (kbd "C-c )") 'reftex-citation)
  (define-key org-mode-map (kbd "C-c (") 'org-mode-reftex-search))
(add-hook 'org-mode-hook 'org-mode-reftex-setup)


;;
;; End of org

