;;
;; Heikki Lehvaslaiho 
;; Time-stamp: <2013-04-24 16:08:02 heikki> 
;;
;; spell checker
;;


 ;; Set aspell as spell program
 (setq ispell-program-name "aspell")

 ;; Speed up aspell: ultra | fast | normal
 (setq ispell-extra-args '("--sug-mode=normal"))

 ;; Flyspell activation for text mode
 (add-hook 'text-mode-hook 
	   (lambda () (flyspell-mode 1)))

 ;; Remove Flyspell from some sub modes of text mode
 (dolist (hook '(change-log-mode-hook 
		 log-edit-mode-hook))
   (add-hook hook (lambda () (flyspell-mode -1))))

