;;
;; Heikki Lehvaslaiho 
;; Time-stamp: <2013-09-12 15:05:26 heikki> 
;;
;;
;; global
;;

;; (setq perl-tab-to-comment t)           ; repeated pressing of tab comments
(setq inhibit-startup-message t)        ; dont show the GNU splash screen
(mouse-avoidance-mode 'jump)		; jump mouse away when typing
;;(setq visible-bell 1)			  ; turn off bip warnings
(require 'whitespace)                   ; add tool to strip trailing white space.
;;(show-trailing-whitespace t)            ; M-x delete-trailing-whitespace <RET>
;;(indicate-empty-lines t)                ; fringe indication
(auto-compression-mode t)		; Handle .gz files
;;(setq-default indent-tabs-mode nil)	  ; Turn off tab character
(mouse-wheel-mode t)			; Support Wheel Mouse Scrolling
(global-auto-revert-mode 1)             ; Auto refresh buffers

(tool-bar-mode -1)                      ; turn off tool bar with
                                        ; icons; save vertical space
(menu-bar-mode)                         ; I do want to see the menus.
                                        ; No args is same as enable
(set scroll-bar-mode 'right)            ; I do want to see the scroll
                                        ; bar; horizontal space is cheep


;; better find-file and switch buffer using a built-in package
(ido-mode 1)
;; easier to read listing for ido, syntax?
;;(ido-vertical-mode 1)


;; Also auto refresh dired, but be quiet about it
(setq global-auto-revert-non-file-buffers t)
(setq auto-revert-verbose nil)

;; Make dired less verbose
;(require 'dired-details)
;(setq-default dired-details-hidden-string "--- ")
;(dired-details-install)


;; "y or n" instead of "yes or no"
(fset 'yes-or-no-p 'y-or-n-p)

;; Global keys
(global-set-key "\C-x\," 'goto-line)    ; old and good shortcut

;; kill line backward from cursor: C-0 C-k
(global-set-key (kbd "C-<backspace>") (lambda ()
                                        (interactive)
                                        (kill-line 0)))


;; auto use emacs desktop
(load "desktop")
(desktop-load-default)
(desktop-read)




;; Abbreviations stuff:
(setq-default abbrev-mode t)
(cond ((file-exists-p "~/.emacs.d/abbrev_defs")
       (read-abbrev-file "~/.emacs.d/abbrev_defs")))
(setq save-abbrevs t)

;; recentf stuff
(require 'recentf)
(recentf-mode 1)
(setq recentf-max-saved-items 200
      recentf-max-menu-items 35)
(global-set-key "\C-x\ \C-r" 'recentf-open-files)

;; unique buffer names with format name:dir
;; http://emacs-fu.blogspot.com/2009/11/making-buffer-names-unique.html
(require 'uniquify) 
(setq 
  uniquify-buffer-name-style 'post-forward
  uniquify-separator ":")

;; current buffer name
(defun insert-buffer-name ()
  "Inserts file name of the buffer on the current buffer."
  (interactive)
  (insert (buffer-name))
)


;;
;; Backup files: in one directory
;;               removed weekly

;; Enable backup files.
(setq make-backup-files t)
;; Enable versioning 
(setq version-control t)  ;; make numbered backups
(setq backup-by-copying t)
(setq kept-new-versions 6)
(setq kept-old-versions 2)
(setq delete-old-versions t)

;; Save all backup file in this directory.
(setq backup-directory-alist (quote ((".*" . "~/.emacs_backups/"))))


;;;(message "Deleting old backup files...")
;;(let ((week (* 60 60 24 7))
;;      (current (float-time (current-time))))
;;  (dolist (file (directory-files temporary-file-directory t))
;;    (when (and (backup-file-name-p file)
;;               (> (- current (float-time (fifth (file-attributes file))))
;;                  week))
;;      (message "%s" file)
;;      (delete-file file))))


;;
;; Mode line setup
;;
;; ref: http://amitp.blogspot.com/2011/08/emacs-custom-mode-line.html

(setq-default
 mode-line-format
 '(;; Position, including warning for 80 columns
   (:propertize "%4l:" face mode-line-position-face)
   (:eval (propertize "%3c" 'face
                      (if (>= (current-column) 80)
                          'mode-line-80col-face
                        'mode-line-position-face)))
   ;; emacsclient [default -- keep?]
   mode-line-client
   " "
   ;; read-only or modified status
   (:eval
    (cond (buffer-read-only
           (propertize " RO " 'face 'mode-line-read-only-face))
          ((buffer-modified-p)
           (propertize " ** " 'face 'mode-line-modified-face))
          (t "    ")))
   " "
   ;; directory and buffer/file name
   (:propertize (:eval (shorten-directory default-directory 30))
                face mode-line-folder-face)
   (:propertize "%b"
                face mode-line-filename-face)
   ;; narrow [default -- keep?]
   " %n "

   ;; mode indicators:
   ;; vc, recursive edit, major mode, minor modes, process, global
   (vc-mode vc-mode)
   "  %["
   (:propertize mode-name
                face mode-line-mode-face)
   "%] "
   (:eval (propertize (format-mode-line minor-mode-alist)
                      'face 'mode-line-minor-mode-face))
   " "
   (:propertize mode-line-process
                face mode-line-process-face)
   (global-mode-string global-mode-string)

   ))


;; Helper function
(defun shorten-directory (dir max-length)
  "Show up to `max-length' characters of a directory name `dir'."
  (let ((path (reverse (split-string (abbreviate-file-name dir) "/")))
        (output ""))
    (when (and path (equal "" (car path)))
      (setq path (cdr path)))
    (while (and path (< (length output) (- max-length 4)))
      (setq output (concat (car path) "/" output))
      (setq path (cdr path)))
    (when path
      (setq output (concat ".../" output)))
    output))

;; Extra mode line faces
(make-face 'mode-line-read-only-face)
(make-face 'mode-line-modified-face)
(make-face 'mode-line-folder-face)
(make-face 'mode-line-filename-face)
(make-face 'mode-line-position-face)
(make-face 'mode-line-mode-face)
(make-face 'mode-line-minor-mode-face)
(make-face 'mode-line-process-face)
(make-face 'mode-line-80col-face)

(set-face-attribute 'mode-line nil
    :foreground "gray60" :background "gray20"
    :inverse-video nil
    :box '(:line-width 6 :color "gray20" :style nil))
(set-face-attribute 'mode-line-inactive nil
    :foreground "gray80" :background "gray40"
    :inverse-video nil
    :box '(:line-width 6 :color "gray40" :style nil))

(set-face-attribute 'mode-line-read-only-face nil
    :inherit 'mode-line-face
    :foreground "#4271ae"
    :box '(:line-width 2 :color "#4271ae"))
(set-face-attribute 'mode-line-modified-face nil
    :inherit 'mode-line-face
    :foreground "#c82829"
    :background "#ffffff"
    :box '(:line-width 2 :color "#c82829"))
(set-face-attribute 'mode-line-folder-face nil
    :inherit 'mode-line-face
    :foreground "gray60")
(set-face-attribute 'mode-line-filename-face nil
    :inherit 'mode-line-face
    :foreground "#eab700"
    :weight 'bold)
(set-face-attribute 'mode-line-position-face nil
    :inherit 'mode-line-face
    ;:family "Menlo"
    :height 130
)
(set-face-attribute 'mode-line-mode-face nil
    :inherit 'mode-line-face
    :foreground "gray80")
(set-face-attribute 'mode-line-minor-mode-face nil
    :inherit 'mode-line-mode-face
    :foreground "gray40"
    :height 110)
(set-face-attribute 'mode-line-process-face nil
    :inherit 'mode-line-face
    :foreground "#718c00")
(set-face-attribute 'mode-line-80col-face nil
    :inherit 'mode-line-position-face
    :foreground "black" :background "#eab700")


;; --------------------------------------------------------
;; Visual bell in Mac Carbon Emacs is an annoying black square
;; This blinks read background once in the echo area
;; Nice little alternative visual bell; Miles Bader <miles /at/ gnu.org>

(defcustom echo-area-bell-string "*DING* " ;"♪"
  "Message displayed in mode-line by `echo-area-bell' function."
  :group 'user)

(defcustom echo-area-bell-delay 0.1
  "Number of seconds `echo-area-bell' displays its message."
  :group 'user)

;; internal variables
(defvar echo-area-bell-cached-string nil)
(defvar echo-area-bell-propertized-string nil)
(defun echo-area-bell ()
  "Briefly display a highlighted message in the echo-area.
    The string displayed is the value of `echo-area-bell-string',
    with a red background; the background highlighting extends to the
    right margin.  The string is displayed for `echo-area-bell-delay'
    seconds.
    This function is intended to be used as a value of `ring-bell-function'."
  (unless (equal echo-area-bell-string echo-area-bell-cached-string)
    (setq echo-area-bell-propertized-string
          (propertize
           (concat
            (propertize
             "x"
             'display
             `(space :align-to (- right ,(+ 2 (length echo-area-bell-string)))))
            echo-area-bell-string)
           'face '(:background "red")))
    (setq echo-area-bell-cached-string echo-area-bell-string))
  (message echo-area-bell-propertized-string)
  (sit-for echo-area-bell-delay)
  (message ""))
(setq ring-bell-function 'echo-area-bell)


;; ---------------------------------------------------------
;;
;; Emacs iPython Notebook
;; -- installed from packages

;; (require 'ein)




;; ---------------------------------------------------------
;;
;; Command aliases
;;

(defalias 'qrr 'query-replace-regexp)  ; M-C-S %



;;
;; Network connectivity
;;

;;
;; Search google from emacs:  M-x google
;;
;; http://emacsredux.com/blog/2013/03/28/google/
(defun google ()
  "Google the selected region if any, display a query prompt otherwise."
  (interactive)
  (browse-url
   (concat
    "http://www.google.com/search?ie=utf-8&oe=utf-8&q="
    (url-hexify-string (if mark-active
                           (buffer-substring (region-beginning) (region-end))
                         (read-string "Google: "))))))

;;
;; Google contacts
;;
;;
;; (require 'google-contacts)



;;
;; Edit current file as root
;;
;; http://wenshanren.org/?p=298
(defun edit-current-file-as-root ()
  "Edit as root the file associated with the current buffer"
  (interactive)
  (if (buffer-file-name)
      (progn
        (setq file (concat "/sudo:root@localhost:" (buffer-file-name)))
        (find-file file))
    (message "Buffer is not associated to a file.")))


;;
;; Move point back to indentation of beginning of line C-a
;; http://emacsredux.com/blog/2013/05/22/smarter-navigation-to-the-beginning-of-a-line/
;;
(defun smarter-move-beginning-of-line (arg)
  "Move point back to indentation of beginning of line.

Move point to the first non-whitespace character on this line.
If point is already there, move to the beginning of the line.
Effectively toggle between the first non-whitespace character and
the beginning of the line.

If ARG is not nil or 1, move forward ARG - 1 lines first.  If
point reaches the beginning or end of the buffer, stop there."
  (interactive "^p")
  (setq arg (or arg 1))

  ;; Move lines first
  (when (/= arg 1)
    (let ((line-move-visual nil))
      (forward-line (1- arg))))

  (let ((orig-point (point)))
    (back-to-indentation)
    (when (= orig-point (point))
      (move-beginning-of-line 1))))

;; remap C-a to `smarter-move-beginning-of-line'
(global-set-key [remap move-beginning-of-line]
                'smarter-move-beginning-of-line)


;;
;; Calendar default location
;;

(setq calendar-latitude 22.3)
(setq calendar-longitude 39.1)
(setq calendar-location-name "KAUST")
