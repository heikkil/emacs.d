;;
;; Heikki Lehvaslaiho 
;; Time-stamp: <2013-04-24 16:03:01 heikki> 
;;
;;
;;  bioinformatics
;;

;;-------
;; ralee mode is good for RNA alignment editing
(autoload 'ralee-mode "ralee-mode" "Yay! RNA things" t)
(setq auto-mode-alist (cons '("\\.stk$" . ralee-mode) auto-mode-alist))
;;-------
