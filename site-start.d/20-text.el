;;
;; Heikki Lehvaslaiho 
;; Time-stamp: <2013-09-09 10:41:36 heikki> 
;;
;;
;;  text manipulation
;;


;; deft mode modifications
(setq
 deft-extension "org"
 deft-directory "~/Documents/org/deft/"
 deft-use-filename-as-title t
 deft-text-mode 'org-mode)
(global-set-key [f9] 'deft)



;; abbrev mode
;;
;; Type the word you want to use as expansion, and then type ‘C-x a g’
;; and the abbreviation for it.  Note how many words you want to be
;; part of the expansion, and then use C-u 4 C-x a g after “Read the
;; Fine Manual” – four words.

(setq default-abbrev-mode t)
(setq abbrev-file-name               ; tell emacs where to read abbrev
        "~/.emacs.d/abbrev_defs")    ; definitions from...

(setq save-abbrevs t)                ; save abbrevs when files are saved
                                     ; you will be asked before saving

(if (file-exists-p abbrev-file-name) ; reads the abbreviations file
    (quietly-read-abbrev-file))      ;  on startup if the file exists

;;
;; Markdown mode  .md
;;
(autoload 'markdown-mode "markdown-mode.el"
   "Major mode for editing Markdown files" t)
(setq auto-mode-alist
   (cons '("\\.md" . markdown-mode) auto-mode-alist))


;;
;; functions
;;

;; word-count
(defun word-count nil "Count words in buffer" (interactive)
  (shell-command-on-region (point-min) (point-max) "wc -w"))


(defun select-text-in-quote ()
  "http://xahlee.org/emacs/modernization_mark-word.html"
  (interactive)
  (let (b1 b2)
    (skip-chars-backward "^<>([{â€œã€Œã€Žâ€¹Â«ï¼ˆã€ˆã€Šã€”ã€ã€–â¦—ã€˜\"")
    (setq b1 (point))
    (skip-chars-forward "^<>)]}â€ã€ã€â€ºÂ»ï¼‰ã€‰ã€‹ã€•ã€‘ã€—â¦˜ã€™\"")
    (setq b2 (point))
    (set-mark b1)))
(global-set-key (kbd "C-'") 'select-text-in-quote)


 (defun txt2xhtml ()
   (interactive)
   (shell-command-on-region (point) 
	  (mark) "txt2xhtml.pl" nil t))

 (defun txt2para ()
   (interactive)
   (shell-command-on-region (point) 
	  (mark) "txt2para.pl" nil t))


 (defun txt2header ()
   (interactive)
   (shell-command-on-region (point) 
	  (mark) "txt2header.pl" nil t))

 (defun do-mark-down (start end)
    "Invoke the Markdown algorithm on region."
    (interactive "r")
    (shell-command-on-region start end "Markdown.pl" t t))
  (global-set-key "\C-cm" 'do-mark-down) 

 (defun do-smarty-pants (start end)
    "Invoke the SmartyPants algorithm on region."
    (interactive "r")
    (shell-command-on-region start end "SmartyPants.pl" t t))
  (global-set-key "\C-cs" 'do-smarty-pants) 


(global-set-key (kbd "C-c ;") 'iedit-mode)


;; folding of code
;; http://emacs.wordpress.com/2007/01/16/quick-and-dirty-code-folding/
(defun toggle-selective-display ()
  (interactive)
  (set-selective-display (if selective-display nil 1)))

(global-set-key [f7] 'toggle-selective-display)


;; join next line
;; http://whattheemacsd.com/key-bindings.el-03.html
(global-set-key (kbd "M-j")
		(lambda () (interactive) (join-line -1)))





;;
;; NEURON hoc and mod files
;;
;; see: http://www.sterratt.me.uk/progs/neuron/
;; wget http://www.sterratt.me.uk/sites/sterratt.me.uk/files/nrnhoc.el
;; wget http://www.sterratt.me.uk/sites/sterratt.me.uk/files/nmodl.el

(autoload 'nrnhoc-mode "nrnhoc" "Enter NRNHOC mode." t)
(setq auto-mode-alist (cons '("\\.hoc\\'" . nrnhoc-mode) auto-mode-alist))
;; (add-hook 'nrnhoc-mode-hook 'turn-on-font-lock)

(autoload 'nmodl-mode "nmodl" "Enter NMODL mode." t)
(setq auto-mode-alist (cons '("\\.mod\\'" . nmodl-mode) auto-mode-alist))



;;
;; Copy and comment a region or line
;;
;;
(defun copy-and-comment-region (beg end)
  "Insert a copy of the lines in region and comment them.
When transient-mark-mode is enabled, if no region is active then only the
current line is acted upon.

If the region begins or ends in the middle of a line, that entire line is
copied, even if the region is narrowed to the middle of a line.
The copied lines are commented according to mode.

Current position is preserved."
  (interactive "r")
  (let ((orig-pos (point-marker)))
    (save-restriction
      (widen)
      (when (and transient-mark-mode (not (use-region-p)))
        (setq beg (line-beginning-position)
              end (line-beginning-position 2)))

      (goto-char beg)
      (setq beg (line-beginning-position))
      (goto-char end)
      (unless (= (point) (line-beginning-position))
        (setq end (line-beginning-position 2)))

      (goto-char beg)
      (insert-before-markers (buffer-substring-no-properties beg end))
      (comment-region beg end)
      (goto-char orig-pos))))

;;
;; Underline current line
;; https://gist.github.com/n3mo/5366507
;;
(defun underline-text (prefixArgCode)
  "Underlines the current line and moves point to the beginning
of the line directly following the underlining. If
`universal-argument' is called, prompts user for underline
character, otherwise uses the = character."
  (interactive "P")
  (let ((selection (buffer-substring-no-properties
  	    (line-beginning-position)
		    (line-end-position)))
	(under-char
	 (if (equal prefixArgCode nil)
	     "="
	   (read-key-sequence "Char?"))))
    (end-of-line)
    (newline-and-indent)
    (insert (apply 'concat (make-list (length selection) under-char)))
    (next-line 1)
    (beginning-of-line)))
 
;; I use the following binding: C-c u (think C-c "underline")
(global-set-key (kbd "C-c u") 'underline-text)


;;
;; Modify the current hightlight line color from green
;; Emacs Starter Kit turns it automatically on
;;  using defun esk-turn-on-hl-line-mode at in starter-kit-defuns.el
;;
(global-hl-line-mode 1)
(set-face-background hl-line-face "AliceBlue")


;;
;; autoload shell-script-mode on zsh configuration .zsh-files
;; from oh-my-zsh project
;;
(setq auto-mode-alist
      (cons '("\\.zsh$" . shell-script-mode) auto-mode-alist))
