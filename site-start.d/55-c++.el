;;
;; Heikki Lehvaslaiho 
;; Time-stamp: <2013-04-24 16:08:03 heikki> 
;;
;; c++
;;


;; Use the GDB visual debugging mode
(setq gdb-many-windows t)
;; Turn Semantic on
(semantic-mode 1)
;; Try to make completions when not typing
(global-semantic-idle-completions-mode 1)
;; Use the Semantic speedbar additions
(add-hook 'speedbar-load-hook (lambda () (require 'semantic/sb)))
;; Treat .h files as C++ files (instead of C)
(add-to-list 'auto-mode-alist '("\\.h\\'" . c++-mode))
;; Run compile when you press F5
(global-set-key (kbd "<f5>") 'compile)
