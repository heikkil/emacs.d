;;
;; Heikki Lehvaslaiho 
;; Time-stamp: <2013-10-22 09:54:51 heikki> 
;;
;; perl
;;

;; moved from perlbrew to plenv
(require 'plenv)
(plenv-global "5.18.1") ;; initialize perl version to use


;;
;; cperl
;;
;; 
;; Use cperl-mode instead of the default perl-mode
(add-to-list 'auto-mode-alist '("\\.\\([pP][Llm]\\|al\\)\\'" . cperl-mode))
(add-to-list 'interpreter-mode-alist '("perl" . cperl-mode))
(add-to-list 'interpreter-mode-alist '("perl5" . cperl-mode))
(add-to-list 'interpreter-mode-alist '("miniperl" . cperl-mode))

(add-hook 'cperl-mode-hook 'n-cperl-mode-hook t)
(defun n-cperl-mode-hook ()
  (setq cperl-indent-level 4
        cperl-close-paren-offset -4
        cperl-continued-statement-offset 4
        cperl-indent-parens-as-block t
        cperl-tab-always-indent t)
  
;;   (setq cperl-indent-level 4)
;;   (setq cperl-continued-statement-offset 4)
;;   (setq cperl-extra-newline-before-brace t)
;;   (set-face-background 'cperl-array-face "wheat")
;;   (set-face-background 'cperl-hash-face "wheat")
  )


;;
;; perltidy
;;

 (defun perltidy ()
    "Run perltidy on the current region or buffer."
    (interactive)
    ; Inexplicably, save-excursion doesn't work here.
    (let ((orig-point (point)))
      (unless mark-active (mark-defun))
      (shell-command-on-region (point) (mark) "perltidy -q" nil t)
      (goto-char orig-point)))

(global-set-key "\C-ct" 'perltidy)

;;
;; pod
;;

(autoload 'pod-mode "pod-mode"
  "Mode for editing POD files" t)
(add-to-list 'auto-mode-alist '("\\.pod$" . pod-mode))
(add-hook 'pod-mode-hook 'font-lock-mode)

(add-hook 'pod-mode-hook '(lambda ( )
                            (progn (font-lock-mode)   ; =syntax highlighting
                                   (auto-fill-mode 1) ; =wordwrap
                                   (flyspell-mode 1)  ; =spellchecking
                                   )))
