;;
;; Heikki Lehvaslaiho 
;; Time-stamp: <2013-04-24 16:02:29 heikki> 
;;
;;
;; magit
;;

; magit short cut
(global-set-key [f8] 'magit-status)
(global-set-key (kbd "C-x g") 'magit-status)

;; full screen magit-status

(defadvice magit-status (around magit-fullscreen activate)
  (window-configuration-to-register :magit-fullscreen)
  ad-do-it
  (delete-other-windows))

(defun magit-quit-session ()
  "Restores the previous window configuration and kills the magit buffer"
  (interactive)
  (kill-buffer)
  (jump-to-register :magit-fullscreen))

;;
;; Relevant emacs packages installed:
;; 
;; magit 
;; git-commit-mode 
;; gitconfig-mode
;; gitignore-mode 
;;
